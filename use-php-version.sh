#!/bin/bash

current_version=$(php -r "echo sprintf('%d.%d', PHP_MAJOR_VERSION, PHP_MINOR_VERSION);")
desired_version=${1?"Usage: $0 <version_number>"}

binaries=('phar' 'phar.phar' 'php' 'php-config' 'phpdbg' 'phpize')
for binary in "${binaries[@]}"; do
    sudo update-alternatives --set "${binary}" /usr/bin/"${binary}${desired_version}" 1> /dev/null
done

sudo a2dismod php"${current_version}" 1> /dev/null
sudo a2enmod php"${desired_version}" 1> /dev/null
sudo systemctl restart apache2 1> /dev/null
