# MISC

Miscellaneous dev related scripts, configuration file examples etc.

### Global .gitignore

To install or update global .gitignore for your local development environment, execute:
```bash
wget https://bitbucket.org/zwebbsrb/misc/raw/master/.gitignore_global -O ~/.gitignore_global
git config --global core.excludesfile ~/.gitignore_global
```

### PHP version switcher

Download, make executable and global.
```bash
wget https://bitbucket.org/zwebbsrb/misc/raw/master/use-php-version.sh -O ~/use-php-version.sh
chmod +x ~/use-php-version.sh
sudo mv ~/use-php-version.sh /usr/bin/use-php-version
```

Then use like:
```bash
use-php-version 7.3
```